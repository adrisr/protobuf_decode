#include <iostream>
#include <string>

using namespace std;

namespace protocol_buffers
{
	class Reader;

	class Field
	{
		public:
			virtual ~Field() {}
			uint64_t id() const { return m_id; }
			void set_id(uint64_t id) { m_id = id; }

			virtual string repr() const = 0;
			
			virtual unique_ptr<Reader> embeddedFields() const 
			{
				return nullptr;
			}
			
		protected:
			Field() 
			{
			}
			
			string header() const 
			{
				string r(to_string(id()));
				return r.append(": ");
			}
			
		private:
			uint64_t m_id = 0;
	};
	
	class Reader
	{
	public:
		
		Reader()
		{
		}

		Reader(const string& base)
			: m_buffer(base)
		{
		}

		void fromStdin()
		{
			m_buffer.clear();
			m_pos = 0;
			char c;
			while (cin.get(c).gcount()>0) 
			{
				m_buffer.push_back(c);
			}
		}
		
		size_t size() const
		{
			return m_buffer.size();
		}
		
		bool eof() const 
		{
			return m_pos >= m_buffer.size();
		}
		
		unique_ptr<Field> nextField();

		// TODO hide :
		//
		uint64_t read_varint()
		{
			uint64_t r = 0;
			unsigned int pos = 0;
			uint8_t byte;
			do
			{
				byte = read_byte();
				r |= (byte & 0x7f) << pos;
				pos += 7;
			}
			while ( (byte&0x80) );

			return r;
		}
		
		uint8_t read_byte()
		{
			if (eof())
				throw "EOF";
			return m_buffer[m_pos++];
		}
		
		string read_raw(size_t len)
		{
			string r;
			r.resize(len);
			for (size_t i=0;i<len;++i)
				r[i] = read_byte();
			return r;
		}
	
	private:
		string m_buffer;
		size_t m_pos = 0;
	};
	
	bool checkValid(const string& data)
	{
		try
		{
			Reader r(data);
			std::unique_ptr<protocol_buffers::Field> field;
			while ( !r.eof() && (field = r.nextField()) );
			return true;
		}
		catch(...)
		{
		}

		return false;
	}

	template<uint8_t NUM_BYTES>
	class UintField : public Field
	{
		public:
		UintField(Reader *reader)
		{
			auto buf = reader->read_raw( NUM_BYTES );
			for (int i = 0 ; i < NUM_BYTES ; ++i)
			{
				m_value |= ((unsigned char)buf[i]) << (i*8);
			}
		}

		virtual string repr() const override
		{
			string r(header());
			return r.append("int")
					.append(to_string(NUM_BYTES * 8))
					.append(" ")
					.append(to_string(m_value));
		}

		private:
			uint64_t m_value = 0;

	};

	class VarintField : public Field
	{
		public:
		VarintField(Reader *reader)
		{
			m_value = reader->read_varint();
		}

		virtual string repr() const override
		{
			string r(header());
			return r.append("int ")
					.append(to_string(m_value));
		}

		private:
			uint64_t m_value;

	};

	class BufferField : public Field
	{
		public:
		BufferField(Reader *reader)
		{
			auto len = reader->read_varint();
			m_data = reader->read_raw(len);
		}
		
		virtual unique_ptr<Reader> embeddedFields() const
		{
			return unique_ptr<Reader>( checkValid(m_data)? new Reader(m_data) : nullptr );
		}

		virtual string repr() const override
		{
			string r(header());
			r.append("buffer[")
			 .append(to_string(m_data.size()))
			 .append("] = \"");
			auto len = m_data.size();
			auto lim = min(len,(size_t)32);
			for (size_t i = 0; i < lim; i++)
			{
				if (m_data[i] >= 0x20 && m_data[i]<= 0x7f)
				{
					r.push_back(m_data[i]);
				}
				else
					r.push_back('?');
			}

			r.append("\"");

			if (len > lim)
			{
				r.append(" ...");
			}

			return r;
		}

		private:
			string m_data;
	};

	std::unique_ptr<Field> Reader::nextField()
	{
		auto head = read_varint();
		unsigned int type = head & 7;
		auto key = head >> 3;
		
		std::unique_ptr<Field> retval;
		
		switch( type ) {
			case 0:
				retval.reset( new VarintField(this) );
				break;
			
			case 1:
				retval.reset( new UintField<8>(this) );
				break;

			case 2:
				retval.reset( new BufferField(this) );
				break;

			case 5:
				retval.reset( new UintField<4>(this) );
				break;

			default:
				throw "Unknown field type error";
		}
		
		retval->set_id(key);

		return retval;
	}

}

void printFields(protocol_buffers::Reader &r, const std::string indent = std::string())
{
	std::unique_ptr<protocol_buffers::Field> field;
	while ( !r.eof() && (field = r.nextField()) )
	{
		std::unique_ptr<protocol_buffers::Reader> embedded = field->embeddedFields();

		if (embedded.get() == nullptr) 
		{
			std::cerr << indent << field->repr() << "\n";
		}
		else
		{
			std::cerr << indent << field->id() << ": {\n";
			auto ni(indent);
			ni.append("   ");
			printFields(*embedded,ni);
			std::cerr << indent << "}\n";
		}
	}
}

int main2()
{
	protocol_buffers::Reader r;
	r.fromStdin();

	std::cerr << "Read a total of " << r.size() << " bytes\n";
	
	printFields(r);

	return 0;
}

int main()
{
	try
	{
		return main2();
	}
	catch(const char *err_msg)
	{
		std::cerr << "Exception caught: " << err_msg << "\n";
	}
	return 33;
}

